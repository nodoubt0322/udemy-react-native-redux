## Install

```bash
git clone https://nodoubt0322@bitbucket.org/nodoubt0322/udemy-react-native-redux.git
npm install
```

Preview online

---

## Router

```js
// navigate to 'home' as defined in your top-level router
Actions.home(PARAMS);

// go back (i.e. pop the current screen off the nav stack)
Actions.pop();

// refresh the current Scene with the specified props
Actions.refresh({ param1: "hello", param2: "world" });
```

Router.js

```jsx
<Scene key="auth">
  <Scene key="login" component={LoginForm} title="Please Login" />
</Scene>

<Scene key="main">
  <Scene
    key="employeeList"
    onRight={() => Actions.employeeCreate()}
    rightTitle="toAdd"
    component={EmployeeList}
    title="Employees"
    initial
  />
  <Scene key="employeeCreate" component={EmployeeCreate} title="Create Employee" />
  <Scene key="employeeEdit" component={EmployeeEdit} title="Edit Employee" />
</Scene>
```

- onRight
  > Called when the right nav bar button is pressed.
- rightTitle
  > Specifies the right button title for scene
- initial
  > Set to true if this is the first scene to display among its sibling Scenes

---

## firebase

init

```js
import firebase from "firebase";

const config = {
  apiKey: "AIzaSyBKL__wKwjSkmN7SKigL38hUD7r_hEc3Oo",
  authDomain: "authentication-98787.firebaseapp.com",
  databaseURL: "https://authentication-98787.firebaseio.com",
  projectId: "authentication-98787",
  storageBucket: "authentication-98787.appspot.com",
  messagingSenderId: "55017054961"
};
!firebase.apps.length ? firebase.initializeApp(config) : firebase.app();
```

usage

```js
//firebase all return firebase.Promise

firebase.auth().onAuthStateChanged(nextOrObserver)
// An observer object or a function triggered on change.

firebase.auth().signInWithEmailAndPassword(email, password)
// sign in with email/password

firebase.auth().createUserWithEmailAndPassword(email, password)
// create user with email/password

firebase.database().ref(`/users/${currentUser.uid}/employees`).push({ name, phone, shift })
// add data to firebase
// ref => Returns a Reference representing the location in the Database corresponding to the provided path
// push() => If you provide a value to push(), the value will be written to the generated location

firebase.database().ref().remove()
// delete data

firebase.database().ref().set(value, cb)
// update data
// This will overwrite any data at this location and all child locations.

firebase.database().ref().update(value, cb)
// selectively update data

firebase.database().ref().on("value", snapshot => console.log(snapshot.val()))
// listen to event whenever "value" change
// available option: "value", "child_added", "child_changed", "child_removed", or "child_moved."

{
  -LFhsXf4I-WIduhxeKb9:{name: "Daniel", phone: "111111", shift: "Friday"},
  -LFhtepOJIDM6BVMBPdT:{name: "jason", phone: "111111", shift: "Saturday"},
  -LFi1F7NZsXpOPuIMia5:{name: "Ssss", phone: "Ssss", shift: "Tuesday"}
}
// save this obj to store
```

---

Actions / Reducers

Since redux's store controlled app's data flow and
firebase will sync data to redux's store whenever data
changed, we just needed to mutate firebase and router.
And one more thing, reset form value whenever create
or update employees.

> employeeUpdate only mutates the store  
> employeeSave update data to firebase

reset form value

```js
const INITIAL_STATE = {
  name: "",
  phone: "",
  shift: ""
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMPLOYEE_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case EMPLOYEE_CREATE:
      return INITIAL_STATE;
    case EMPLOYEE_SAVE_SUCCESS:
      return INITIAL_STATE;
    default:
      return state;
  }
};
```

---

## Modal component

```js
<Confirm
  visible={this.state.showModal}
  onAccept={this.onAccept.bind(this)}
  onDecline={this.onDecline.bind(this)}
>
  Are you sure you want to delete this?
</Confirm>
```

```js
<Modal
  visible={visible}
  transparent
  animationType="slide"
  onRequestClose={() => {}}
>
  <View style={containerStyle}>
    <CardSection style={cardSectionStyle}>
      <Text style={textStyle}>{children}</Text>
    </CardSection>

    <CardSection>
      <Button onPress={onAccept}>Yes</Button>
      <Button onPress={onDecline}>No</Button>
    </CardSection>
  </View>
</Modal>
```

- visible
  > visible , type: boolean
- transparent , type: boolean
  > The transparent prop determines whether your modal will fill the entire view. Setting this to true will render the modal over a transparent background
- animationType
  > 3 animation types: slide, fade, none
- onRequestClose
  > The onRequestClose callback is called when the user taps the hardware back button on Android or the menu button on Apple TV
  > Required for Android / TVOS

---

## componentWillReceiveProps

> This method is called when props are passed to the Component instance

```js
  componentWillMount() {
    this.props.employeesFetch();
    this.createDataSource(this.props);
  }
  componentWillReceiveProps(nextProps) {
    this.createDataSource(nextProps);
  }
  createDataSource({ employees }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSource = ds.cloneWithRows(employees);
  }
```

---

## react-native-communications

```js
import Communications from "react-native-communications";
Communications.text(phoneNumber, content);
```

> Doesn't support simulator
