import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import firebase from "firebase";
import ReduxThunk from "redux-thunk";
import reducers from "./src/reducers";
import Router from "./src/Router";

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

export default class App extends Component {
  state = { loggedIn: null };

  componentWillMount() {
    const config = {
      apiKey: "AIzaSyBKL__wKwjSkmN7SKigL38hUD7r_hEc3Oo",
      authDomain: "authentication-98787.firebaseapp.com",
      databaseURL: "https://authentication-98787.firebaseio.com",
      projectId: "authentication-98787",
      storageBucket: "authentication-98787.appspot.com",
      messagingSenderId: "55017054961"
    };

    !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();

    firebase.auth().onAuthStateChanged(user => {
      console.log({ user });
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }

  render() {
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}
